
//-----------------------------------------------------------------------------
/** @file RichConverter.cpp
 *
 *  Implementation file for class : Rich::Converter
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2009-07-07
 */
//-----------------------------------------------------------------------------

// local
#include "RichFutureKernel/RichConverter.h"

// ============================================================================
// Force creation of templated classes
#include "RichCommonBase.icpp"
template class Rich::Future::CommonBase< Rich::Future::Converter_Imp > ;
// ============================================================================
