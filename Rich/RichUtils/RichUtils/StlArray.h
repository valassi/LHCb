
//================================================================
/** @file StlArray.h
 *
 *  Header file for std::array + Additional Gaudi methods
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2015-02-07
 */
//================================================================

#pragma once

// STL
#include <array>

// Gaudi
#include "GaudiKernel/ToStream.h"
