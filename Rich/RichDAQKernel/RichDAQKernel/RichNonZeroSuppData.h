
//-----------------------------------------------------------------------------
/** @file RichNonZeroSuppData.h
 *
 *  Header file for RICH Alice mode non-ZS data formats
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-07
 */
//-----------------------------------------------------------------------------

#pragma once

// version one
#include "RichDAQKernel/RichNonZeroSuppData_V1.h"

// version two
#include "RichDAQKernel/RichNonZeroSuppData_V2.h"

// version three
#include "RichDAQKernel/RichNonZeroSuppData_V3.h"
