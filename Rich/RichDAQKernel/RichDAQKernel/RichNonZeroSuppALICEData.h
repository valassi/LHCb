
//-----------------------------------------------------------------------------
/** @file RichNonZeroSuppALICEData.h
 *
 *  Header file for RICH DAQ utility class : RichNonZeroSuppALICEData
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-11-07
 */
//-----------------------------------------------------------------------------

#pragma once

#include "RichDAQKernel/RichNonZeroSuppALICEData_V1.h"
#include "RichDAQKernel/RichNonZeroSuppALICEData_V2.h"
