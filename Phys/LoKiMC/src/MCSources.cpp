// ============================================================================
// Include files
// ============================================================================
#include <algorithm>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IToolSvc.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/MCSources.h"
#include "LoKi/MCAlgs.h"
#include "LoKi/MCExtract.h"
#include "LoKi/Services.h"
#include "LoKi/MCParticles1.h"
#include "LoKi/Trees.h"
#include "LoKi/Algs.h"
// ============================================================================
/** @file
 *  Implementation file for various sources
 *  @author Vanya BELYAEV ibelyav@physics.syr.edu
 *  @date 2006-12-07
 */
namespace LoKi {

namespace MCParticles {
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&           path ,
  IDataProviderSvc*            svc  )
  : LoKi::AuxFunBase ( std::tie ( path ) )
  , SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant ( true ) )
  , m_use_finder ( false     )
  , m_finder     ( Decays::Trees::Invalid_<const LHCb::MCParticle*>() )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&           path ,
  const LoKi::MCTypes::MCCuts& cuts ,
  IDataProviderSvc*            svc  )
  : LoKi::AuxFunBase ( std::tie ( path , cuts ) )
  , SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( cuts      )
  , m_use_finder ( false     )
  , m_finder     ( Decays::Trees::Invalid_<const LHCb::MCParticle*>() )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&           path ,
  const Decays::iNode&         node ,
  IDataProviderSvc*            svc  )
  : SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( DecNode ( node ) )
  , m_use_finder ( false     )
  , m_finder     ( Decays::Trees::Invalid_<const LHCb::MCParticle*>() )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&              path   ,
  const Decays::IMCDecay::Finder& finder ,
  IDataProviderSvc*               svc  )
  : SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant ( false ) )
  , m_use_finder ( true      )
  , m_finder     ( finder    )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&              path  ,
  const Decays::IMCDecay::iTree&  tree  ,
  IDataProviderSvc*               svc   )
  : SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant ( false ) )
  , m_use_finder ( true      )
  , m_finder     ( tree      )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&  path       ,
  const std::string&  descriptor ,
  IDataProviderSvc*   svc        )
  : LoKi::AuxFunBase ( std::tie ( path , descriptor ) )
  , m_path       ( path       )
  , m_dataSvc    ( svc        )
  , m_cut        ( LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant ( false ) )
  , m_use_finder ( true       )
  , m_finder     ( Decays::Trees::Invalid_<const LHCb::MCParticle*>() )
  , m_decay      ( descriptor )
{
  //
  if ( gaudi() ) { buildFinder () ; }
}
// ============================================================================
void SourceTES::buildFinder () const
{
  //
  if ( !use_finder() || m_finder.valid() ) { return ; }
  //
  LoKi::ILoKiSvc* ls = lokiSvc() ;
  SmartIF<IToolSvc> toolSvc ( ls ) ;
  Assert ( !(!toolSvc) , "Unable to locate tool service" ) ;
  //
  std::string factory = "LoKi::MCDecay" ;
  //
  Decays::IMCDecay* tool = nullptr ;
  StatusCode sc = toolSvc -> retrieveTool ( factory , tool ) ;
  Assert ( sc.isSuccess () , "Unable to retrieve '" + factory + "'" , sc ) ;
  Assert ( nullptr != tool       , "Decays::IMCDecay* points to NULL" ) ;
  //
  m_finder = tool -> tree ( m_decay) ;
  //
  toolSvc -> releaseTool ( tool ) ; // do not need the tool anymore
  //
  Assert ( !(!m_finder) , "the tree is invalid:  '" + m_decay + "'" ) ;
}
// ============================================================================
//  MANDATORY: clone method ("virtual constructor")
// ============================================================================
SourceTES* SourceTES::clone() const { return new SourceTES ( *this ) ; }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LHCb::MCParticle::ConstVector SourceTES::operator() () const {
  //
  LHCb::MCParticle::ConstVector result ;
  //
  get ( m_path , result ) ;
  //
  return result ;
}
// ============================================================================
// get the data from TES
// ============================================================================
std::size_t SourceTES::get ( const std::string&             location ,
                             LHCb::MCParticle::ConstVector& output   ) const
{
  //
  if ( !m_dataSvc )
  {
    const LoKi::Services& svcs = LoKi::Services::instance() ;
    m_dataSvc = svcs.evtSvc() ;
    Assert ( m_dataSvc,
             "Could not locate valid IDataProvidrSvc" ) ;
  }
  //
  SmartDataPtr<LHCb::MCParticle::Container> events ( m_dataSvc , location ) ;
  //
  const LHCb::MCParticle::Container* parts = events ;
  Assert ( nullptr != parts , "No valid data is found at location '"+location+"'") ;
  //
  if ( m_use_finder )
  {
    if ( !m_finder && !m_decay.empty() ) { buildFinder() ; }
    if ( !m_finder )
    {
      const LoKi::Services& svcs = LoKi::Services::instance() ;
      StatusCode sc = m_finder.validate ( svcs.ppSvc() ) ;
      Assert ( sc.isSuccess () , "Unable to validate the DecayFinder" ) ;
    }
    //
    Assert   ( !(!m_finder)    , "Unable to validate the DecayFinder" ) ;
    // use finder!!
    m_finder.findDecay ( parts->begin () ,
                         parts->end   () ,
                         output          ) ;
  }
  else
  {
    // use cuts!
    std::copy_if( parts->begin ()  , parts->end   ()  ,
                  std::back_inserter ( output ) , std::cref(m_cut) ) ;
  }
  //
  return output.size() ;
}
// ============================================================================
// get the data from TES
// ============================================================================
std::size_t SourceTES::count ( const std::string&             location  ) const
{
  //
  if ( !m_dataSvc )
  {
    const LoKi::Services& svcs = LoKi::Services::instance() ;
    m_dataSvc = svcs.evtSvc() ;
    Assert ( m_dataSvc,
             "Could not locate valid IDataProvidrSvc" ) ;
  }
  //
  SmartDataPtr<LHCb::MCParticle::Container> events ( m_dataSvc , location ) ;
  //
  const LHCb::MCParticle::Container* parts = events ;
  Assert ( nullptr != parts , "No valid data is found at location '"+location+"'") ;
  //
  if ( m_use_finder )
  {
    LHCb::MCParticle::ConstVector output ;
    if ( !m_finder && !m_decay.empty() ) { buildFinder() ; }
    if ( !m_finder )
    {
      const LoKi::Services& svcs = LoKi::Services::instance() ;
      StatusCode sc = m_finder.validate ( svcs.ppSvc() ) ;
      Assert ( sc.isSuccess () , "Unable to validate the DecayFinder" ) ;
    }
    //
    Assert   ( !(!m_finder)    , "Unable to validate the DecayFinder" ) ;
    // use finder!!
    m_finder.findDecay ( parts->begin () ,
                         parts->end   () ,
                         output          ) ;

    return output.size() ;
  }
  //
  // use cuts!
  //
  return LoKi::Algs::count_if ( parts->begin ()  ,
                                parts->end   ()  , m_cut ) ;
  //
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& SourceTES::fillStream ( std::ostream& o ) const
{
  o << " MCSOURCE( '" << m_path << "' , " ;
  if  ( m_use_finder ) { o << m_finder ; }
  else                 { o << m_cut    ; }
  //
  return o << " ) " ;
}



// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
TESCounter::TESCounter ( const std::string&           path ,
                         const LoKi::MCTypes::MCCuts& cuts )
  : LoKi::AuxFunBase ( std::tie ( path , cuts ) )
  , m_source ( path , cuts )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
TESCounter::TESCounter ( const std::string&           path ,
                         const Decays::iNode&         node )
  : m_source ( path , node )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
TESCounter::TESCounter ( const std::string&              path ,
                         const Decays::IMCDecay::Finder& finder )
  : m_source ( path , finder )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
TESCounter::TESCounter ( const std::string&              path ,
                         const Decays::IMCDecay::iTree&  finder )
  : m_source ( path , finder )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
TESCounter::TESCounter ( const std::string&              path ,
                         const std::string&              finder )
  : LoKi::AuxFunBase ( std::tie ( path , finder ) )
  , m_source ( path , finder )
{}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
TESCounter* TESCounter::clone() const { return new TESCounter ( *this ) ; }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
double TESCounter::operator() (  ) const
{ return m_source.count ( m_source.path() ) ; }
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& TESCounter::fillStream ( std::ostream& o ) const
{
  o << " MCNUM( '" << m_source.path() << "' , " ;
  if  ( m_source.use_finder() ) { o << m_source.finder() ; }
  else                          { o << m_source.cut   () ; }
  //
  return o << " ) " ;
}

}

namespace MCVertices {
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&           path ,
  IDataProviderSvc*            svc  )
  : LoKi::AuxFunBase ( std::tie ( path ) )
  , SourceTES::_Source ()
  , m_path       ( path      )
  , m_dataSvc    ( svc       )
  , m_cut        ( LoKi::BasicFunctors<const LHCb::MCVertex*>::BooleanConstant ( true ) )
{}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
SourceTES::SourceTES
( const std::string&            path ,
  const LoKi::MCTypes::MCVCuts& cuts ,
  IDataProviderSvc*             svc  )
  : LoKi::AuxFunBase ( std::tie ( path , cuts ) )
  , SourceTES::_Source ()
  , m_path       ( path )
  , m_dataSvc    ( svc  )
  , m_cut        ( cuts )
{}
// ============================================================================
//  MANDATORY: clone method ("virtual constructor")
// ============================================================================
SourceTES* SourceTES::clone() const { return new SourceTES ( *this ) ; }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LHCb::MCVertex::ConstVector SourceTES::operator() () const
{
  if ( !m_dataSvc )
  {
    const LoKi::Services& svcs = LoKi::Services::instance() ;
    m_dataSvc = svcs.evtSvc() ;
    Assert ( m_dataSvc,
             "Could not locate valid IDataProvidrSvc" ) ;
  }
  //
  SmartDataPtr<LHCb::MCVertex::Container> events ( m_dataSvc , m_path ) ;
  //
  const LHCb::MCVertex::Container* parts = events ;
  Assert ( parts , "No valid data is found at location '"+m_path+"'") ;
  //
  LHCb::MCVertex::ConstVector result ;
  // use cuts!
  std::copy_if( parts->begin ()  ,
                parts->end   ()  ,
                std::back_inserter ( result ) , std::cref(m_cut) ) ;
  //
  return result ;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& SourceTES::fillStream ( std::ostream& o ) const
{ return o << " MCVSOURCE( '" << m_path << "', " << m_cut << " ) " ; }

}
}
// ============================================================================
// The END
// ============================================================================
