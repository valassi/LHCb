#ifndef MUONDET_CLID_MUONSTATIONCABLING_H 
#define MUONDET_CLID_MUONSTATIONCABLING_H 1
#include "GaudiKernel/ClassID.h"

/// Class ID of the Muon readout
static const CLID CLID_MuonStationCabling = 11090;  

#endif // MUONDET_CLID_MUONSTATIONCABLING_H
