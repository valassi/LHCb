#ifndef MUONDET_CLID_MUONODEBOARD_H 
#define MUONDET_CLID_MUONODEBOARD_H 1
#include "GaudiKernel/ClassID.h"

/// Class ID of the Muon readout
static const CLID CLID_MuonODEBoard = 11092;  

#endif // MUONDET_CLID_MUONODEBOARD_H
