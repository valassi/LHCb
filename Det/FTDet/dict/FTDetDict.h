#ifndef DICT_FTDETDICT_H 
#define DICT_FTDETDICT_H 1

#include "FTDet/DeFTDetector.h"
#include "FTDet/DeFTStation.h"
#include "FTDet/DeFTLayer.h"
#include "FTDet/DeFTQuarter.h"
#include "FTDet/DeFTModule.h"
#include "FTDet/DeFTMat.h"

#endif // DICT_FTDETDICT_H
