#ifndef      __DETDESC_SOLID_SOLIDADVANCED_H__
#define      __DETDESC_SOLID_SOLIDADVANCED_H__


//
// boolean solids 
// 
#include "DetDesc/SolidSubtraction.h"
#include "DetDesc/SolidIntersection.h"
#include "DetDesc/SolidUnion.h"


#endif   //  __DETDESC_SOLID_SOLIDADVANCED_H__
