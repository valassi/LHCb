// ============================================================================
#ifndef LHCBMATH_BERNSTEIN_H
#define LHCBMATH_BERNSTEIN_H 1
// ============================================================================
// Include files
// ============================================================================
#include "LHCbMath/BernsteinPoly.h"
#include "LHCbMath/Bernstein1D.h"
#include "LHCbMath/Bernstein2D.h"
#include "LHCbMath/Bernstein3D.h"
// ============================================================================
/** @file LHCbMath/Bernstein.h
 *  Set of useful math-functions, related to Bernstein polynomials
 *
 *  @see http://en.wikipedia.org/wiki/Bernstein_polynomial
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2010-04-19
 *
 */
// ============================================================================
#endif // LHCBMATH_BERNSTEIN_H
// ============================================================================
